#pragma once

/*главная функция где будет реализован вывод всего на экран*/
void createDisplay();

/*простой двумерный массив*/
void tDArray(const int ROWS, const int COLS, int number);

/*передача аргрументов функции по указателю*/
void funcPArgs(int *pA, int *pB, int *pC);

/*пример перегрузки функции в классе*/
class Overloaded
{
public:
    int Sum(int a, int b)
    {
        return a + b;
    }

    double Sum(double a, double b)
    {
        return a + b;
    }
};

/*рекурсия на примере факториала*/
int Fact(int N);

/*создание и вывод простого указателя*/
void defPointer(int x);

/*создание и вывод простого массива с использованием указателя*/
void defPointArray(int size, int number);

/*создание, вывод на экран простого массива*/
void createAPrintArray(int size, int number);

/*создание, вывод на экран и удаление простого динамического массива*/
void createAPrintDynArray(int size);

/*создание, вывод на экран и удаление двухмерного динамического массива*/
void createAPrintDynArray(int rows, int cols, int number);