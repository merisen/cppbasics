#include <iostream>
#include "fuck.h"

/**
 *
 * Чтоб запустить тот или иной код нужно просто убрать однострочный комментарий с #define <NAME>
 *
 * */

//#define ARRAYS /*примеры массивов*/
//#define POINTERS /*примеры указателей и ссылок*/
//#define POINTER_ARR /*примеры указателей и ссылок с массивом*/
//#define FACT_FUNC /*рекурсия на примере факториала*/
//#define OVERLOADED /*пример перегрузки функции в классе, класс реализован в func.h*/
//#define PARGS /*передача аргрументов функции по указателю*/
//#define TDARRAY /*простой двумерный массив*/

/*главная функция где реализован вывод всего на экран*/
void createDisplay()
{
#ifdef ARRAYS
    int size = 0, num = 0;
    std::cout << "Enter size of array: "; std::cin >> size;
    std::cout << "Enter number: "; std::cin >> num;

    std::cout << "\n";
    /*простой массив*/
    createAPrintArray(size, num);
    std::cout << "\n";
    /*простой динамический массив*/
    createAPrintDynArray(size);
    std::cout << "\n";
    /*простой двухмерный массив*/
    createAPrintDynArray(size, size, num);
#endif

#ifdef POINTERS
    int x = 0;
    std::cout << "Enter number 'x': "; std::cin >> x;
    defPointer(x);
#endif

#ifdef POINTER_ARR
    int size = 0, number = 0;
    std::cout << "Enter size of array: "; std::cin >> size;
    std::cout << "Enter number: "; std::cin >> number;

    defPointArray(size, number);
#endif

#ifdef FACT_FUNC
    int i = 0;
    std::cout << "Enter number: "; std::cin >> i;
    std::cout << Fact(i) << std::endl;
#endif

#ifdef OVERLOADED
    Overloaded someFun;
    std::cout << someFun.Sum(5, 5) << std::endl;
    std::cout << someFun.Sum(6.6, 6.6) << std::endl;
#endif

#ifdef PARGS
    /*тут можно вписать свои данные если хотите, мне стало лень делать cout и cin*/
    int a = 0, b = 0, c = 1;
    std::cout << a << "\t" << b << "\t" << c << std::endl;
    funcPArgs(&a, &b, &c);
    std::cout << a << "\t" << b << "\t" << c << std::endl;
#endif

#ifdef TDARRAY
    int r = 0, c = 0, num = 0;
    std::cout << "r = "; std::cin >> r;
    std::cout << "c = "; std::cin >> c;
    std::cout << "num = "; std::cin >> num;

    tDArray(r, c, num);
#endif
}

/*простой двумерный массив*/
void tDArray(const int ROWS, const int COLS, int number)
{
    int arr[ROWS][COLS];
    for (int i = 0; i < ROWS; i++)
    {
        for (int j = 0; j < COLS; j++)
        {
            arr[i][j] = rand() % number;
            std::cout << arr[i][j];
        }
        std::cout << "" << std::endl;
    }
}

/*передача аргрументов функции по указателю*/
void funcPArgs(int *pA, int *pB, int *pC)
{
    (*pA) = 555;
    (*pB)++;
    (*pC) -= 20;
}

/*рекурсия на примере факториала*/
int Fact(int N)
{
    if(N == 0) return 0;
    if(N == 1) return 1;

    return N * Fact(N -1);
}

/*создание и вывод простого указателя*/
void defPointer(int x)
{
    int *pX = &x;
    int *pX2 = &x;

    std::cout << "x =\t" << x << std::endl;
    std::cout << "Default pointers:" << std::endl;
    std::cout << "pX =\t" << pX << std::endl;
    std::cout << "pX2 =\t" << pX2 << std::endl;

    std::cout << "'Normal default' pointers:" << std::endl;
    std::cout << "pX =\t" << *pX << std::endl;
    std::cout << "pX2 =\t" << *pX2 << std::endl;
}

/*создание и вывод простого массива с использованием указателя*/
void defPointArray(int size, int number)
{
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        arr[i] = rand() % number;
        std::cout << arr[i] << std::endl;
    }
    int *pArr = arr;

    std::cout << "Array 'arr': " << arr << std::endl;
    std::cout << "Array 'pArr': " << pArr << std::endl;

    for (int j = 0; j < size; j++)
    {
        std::cout << pArr[j] << std::endl;
    }
}

/*создание, вывод на экран простого массива*/
void createAPrintArray(int size, int number)
{
    int arr[size];
    for(int i = 0; i < size; i++)
    {
        arr[i] = rand() % number;
        std::cout << arr[i] << "\t";
    }
}

/*создание, вывод на экран и удаление простого динамического массива*/
void createAPrintDynArray(int size)
{
    int *arr = new int[size];

    for (int i = 0; i < size; i++)
    {
        arr[i] = rand() % 5;
        std::cout << arr[i] << "\t" << arr + i << std::endl;
    }

    delete[] arr;
}

/*создание, вывод на экран и удаление двухмерного динамического массива*/
void createAPrintDynArray(int rows, int cols, int number)
{
    int **arr = new int*[rows];

    for(int i = 0; i < rows; i++)
    {
        arr[i] = new int[cols];
    }
    for (int j = 0; j < rows; j++)
    {
        for (int i = 0; i < cols; i++)
        {
            arr[j][i] = rand() % number;
            std::cout << arr[j][i] << "\t";
        }
        std::cout << std::endl;
    }
    for(int i = 0; i < rows; i++)
    {
        delete[] arr[i];
    }
    delete[] arr;
}